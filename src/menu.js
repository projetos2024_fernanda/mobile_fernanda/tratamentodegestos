import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";

export default function Menu({ navigation }) {
  return (
    <View>
      <TouchableOpacity onPress={() => navigation.navigate("CountMoviment")}>
        <Text>Count</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate("ImageMoviment")}>
        <Text>Imagem</Text>
      </TouchableOpacity>
    </View>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     flexDirection: "column",
//     justifyContent: "center",
//     alignItems: "center",
//   },
//   menu: {
//     padding: 10,
//     margin: 5,
//     backgroundColor: "pink",
//     borderRadius: 5,
//   },
// });
