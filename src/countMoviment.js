import react, { useRef,useState,useEffect}  from "react";
import { View, Dimensions, PanResponder, Button, Text } from "react-native";

const CountMoviment = () =>{
   const [count,setCount] = useState(0);

   const screenHeight = Dimensions.get("window").height
    //Ver a dimensão da tela (tamanho)
   const gestureThreshold = screenHeight * 0.25;
    //Threshold = valor a ser atingido. Nesse caso é o valor da tela 25%. Margem limite.
   const panResponder = useRef(
      PanResponder.create({
         onStartShouldSetPanResponder: () => true,
         onPanResponderMove: (event, gestureState) => {},
         onPanResponderRelease: (event, gestureState) => {
            if (gestureState.dy < -gestureThreshold) {
               setCount((prevCount)=>prevCount+1) 
         } 
      
      }
   })
   ).current;

     return(
        <View{...panResponder.panHandlers} 
        style={{ flex:1,alignItems:"center",justifyContent:"center"}}>
            <Text>Valor do Contador: {count}</Text>        
         </View>
     );
    };
    
    export default CountMoviment;